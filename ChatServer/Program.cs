﻿using Serializator;
using System;
using System.Threading;

namespace ChatServer
{
    public class ServerSettings
    {
        public int PortTcp { get; set; }
    }

    public class Program
    {
        static ServerObject server; // сервер
        static Thread listenThread; // потока для прослушивания
        public static ServerSettings ServerSettings;

        private static void Main()
        {
            //ServerSettings = new ServerSettings { PortTcp = 8888 };
            //SerializationSettings<ServerSettings>.SaveSettingsToFile(ServerSettings); 

            ServerSettings = SerializationSettings<ServerSettings>.GetSettingsFromFile();

            Console.WriteLine(@"PortTcp={0}", ServerSettings.PortTcp);

            if (ServerSettings == null)
            {
                throw new Exception("Конфиг settings.json для сервера не найден");                
            }              

            try
            {
                server = new ServerObject();
                listenThread = new Thread(new ThreadStart(server.Listen));
                listenThread.Start(); //старт потока
            }
            catch (Exception ex)
            {
                server.Disconnect();
                Console.WriteLine(ex.Message);
            }
        }
    }
}