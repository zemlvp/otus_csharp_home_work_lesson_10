﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading;
using Serializator;

namespace ChatServer
{
    public class ServerObject
    {
        static TcpListener tcpListener; // сервер для прослушивания
        readonly List<ClientObject> clients = new List<ClientObject>(); // все подключения

        protected internal void AddConnection(ClientObject clientObject)
        {
            clients.Add(clientObject);
        }

        protected internal void RemoveConnection(string id)
        {
            // получаем по id закрытое подключение
            ClientObject client = clients.FirstOrDefault(c => c.Id == id);
            // и удаляем его из списка подключений
            if (client != null)
                clients.Remove(client);
        }

        // прослушивание входящих подключений
        protected internal void Listen()
        {
            try
            {
                tcpListener = new TcpListener(IPAddress.Any, Program.ServerSettings.PortTcp);
                tcpListener.Start();
                Console.WriteLine("Сервер запущен. Ожидание подключений...");

                while (true)
                {
                    TcpClient tcpClient = tcpListener.AcceptTcpClient();

                    ClientObject clientObject = new ClientObject(tcpClient, this);
                    Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                    clientThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Disconnect();
            }
        }

        // трансляция сообщения одному клиенту
        public void NotifyClient(string clientId, Message message)
        {
            var jsonMessage = Serialization.Serialize(message);
            byte[] data = Encoding.Unicode.GetBytes(jsonMessage);
            var client = clients.FirstOrDefault(c => c.Id == clientId);
            client.Stream.Write(data, 0, data.Length); //передача данных
        }

        // трансляция сообщения подключенным клиентам
        protected internal void BroadcastMessage(Message message)
        {
            var jsonMessage = Serialization.Serialize(message);
            byte[] data = Encoding.Unicode.GetBytes(jsonMessage);
            for (int i = 0; i < clients.Count; i++)
            {
                if (clients[i].Id != message.ClientId)   // если id клиента не равно id отправляющего
                {
                    clients[i].Stream.Write(data, 0, data.Length); //передача данных
                }
                //else
                //{
                //    #region обработка ошибки для юзера test 
                //    if (message.UserName == "test" && !message.Text.Contains("вошел в чат"))
                //    {
                //        jsonMessage = Serialization.Serialize(message);
                //        data = Encoding.Unicode.GetBytes(jsonMessage);
                //        clients[i].Stream.Write(data, 0, data.Length); 
                //    }
                //    #endregion
                //}
            }
        }
        // отключение всех клиентов
        protected internal void Disconnect()
        {
            tcpListener.Stop(); //остановка сервера

            for (int i = 0; i < clients.Count; i++)
            {
                clients[i].Close(); //отключение клиента
            }
            Environment.Exit(0); //завершение процесса
        }
    }
}
