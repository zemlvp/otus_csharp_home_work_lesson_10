﻿using Serializator;
using System;
using System.Net.Sockets;
using System.Text;

namespace ChatServer
{
    public class ClientObject
    {
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream { get; private set; }
        readonly TcpClient client;
        readonly ServerObject server;

        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            serverObject.AddConnection(this);
        }

        public void Process()
        {
            try
            {
                Stream = client.GetStream();

                var message = GetMessage();

                message = new Message
                {
                    ClientId = this.Id,
                    UserName = message.UserName,
                    Text = message.UserName + " вошел в чат"
                };

                // посылаем сообщение о входе в чат всем подключенным пользователям
                server.BroadcastMessage(message);

                Console.WriteLine(message.Text);

                // в бесконечном цикле получаем сообщения от клиента
                while (true)
                {
                    try
                    {
                        message = GetMessage();

                        message.ClientId = this.Id;

                        if (message.ErrorMessage == null)
                        {
                            message.Text = string.Format("{0}: {1}", message.UserName, message.Text);
                            Console.WriteLine(message.Text);
                            server.BroadcastMessage(message);
                        }
                        else
                        {
                            ShowMessageError(message);
                            server.NotifyClient(this.Id, message);
                        }
                    }
                    catch
                    {
                        message.Text = string.Format("{0}: покинул чат", message.UserName);
                        Console.WriteLine(message.Text);
                        server.BroadcastMessage(message);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // в случае выхода из цикла закрываем ресурсы
                server.RemoveConnection(this.Id);
                Close();
            }
        }

        // чтение входящего сообщения и преобразование в строку
        private Message GetMessage()
        {
            Message message = null;

            try
            {
                byte[] data = new byte[64]; // буфер для получаемых данных
                StringBuilder builder = new StringBuilder();
                do
                {
                    int bytes = Stream.Read(data, 0, data.Length);
                    builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                }
                while (Stream.DataAvailable);

                message = Serialization.Deserialize(builder.ToString());

                #region Имитация любой ошибки для юзера test на стороне сервера
                if (message.UserName == "test")
                    throw new Exception();
                #endregion
            }
            catch
            {
                message.ErrorMessage = new ErrorMessage
                {
                    message = "Произошла ошибка",
                    code = "500"
                };
            }

            return message;
        }

        protected internal void Close()
        {
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
        }

        private void ShowMessageError(Message message)
        {
            message.ClientId = this.Id;
            Console.WriteLine(string.Format("clienID: {0}, code: {1}, message: {2}", message.ClientId, message.ErrorMessage.code, message.ErrorMessage.message));
        }
    }
}