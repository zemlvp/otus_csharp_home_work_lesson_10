﻿using System;
using System.Threading;
using System.Net.Sockets;
using System.Text;
using Serializator;

namespace ChatClient
{
    class ClientSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }
    class Program
    {
        static string userName;
        static TcpClient client;
        static NetworkStream stream;

        static void Main()
        {
            //var settings1 = new ClientSettings
            //{
            //    Host = "127.0.0.1",
            //    Port = 8888
            //};

            //SerializationSettings<ClientSettings>.SaveSettingsToFile(settings1);

            var clientSettings = SerializationSettings<ClientSettings>.GetSettingsFromFile();

            Console.WriteLine(@"host={0}, port={1}", clientSettings.Host, clientSettings.Port);

            if (clientSettings == null)
                throw new Exception("Конфиг settings.json для клиента не найден");
 
            Console.Write("Введите свое имя: ");
            userName = Console.ReadLine();
            client = new TcpClient();
            try
            {
                client.Connect(clientSettings.Host, clientSettings.Port);  //подключение клиента
                stream = client.GetStream(); // получаем поток

                Message message = new Message { UserName = userName, Text = userName };
                var jsonMessage = Serialization.Serialize(message);
                byte[] data = Encoding.Unicode.GetBytes(jsonMessage);
                stream.Write(data, 0, data.Length);

                // запускаем новый поток для получения данных
                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start();
                Console.WriteLine("Добро пожаловать, {0}", userName);
                SendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }

        // отправка сообщений
        static void SendMessage()
        {
            Console.WriteLine("Введите сообщение: ");

            while (true)
            {
                var message = new Message { UserName = userName, Text = Console.ReadLine() };
                var jsonMessage = Serialization.Serialize(message);
                byte[] data = Encoding.Unicode.GetBytes(jsonMessage);
                stream.Write(data, 0, data.Length);
            }
        }

        // получение сообщений
        static void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] data = new byte[64]; // буфер для получаемых данных
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);

                    string jsonMessage = builder.ToString();

                    var message = Serialization.Deserialize(jsonMessage);

                    if (message.ErrorMessage == null)
                        Console.WriteLine(message.Text);  //вывод сообщения
                    else
                        Console.WriteLine(string.Format("clienID: {0}, code: {1}, message: {2}", message.ClientId, message.ErrorMessage.code, message.ErrorMessage.message));
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!"); //соединение было прервано
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        static void Disconnect()
        {
            if (stream != null)
                stream.Close();//отключение потока
            if (client != null)
                client.Close();//отключение клиента
            Environment.Exit(0); //завершение процесса
        }
    }
}
