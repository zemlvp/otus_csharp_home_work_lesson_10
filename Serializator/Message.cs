﻿namespace Serializator
{
    public class Message
    {
        public string ClientId { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        public ErrorMessage ErrorMessage { get; set; }
    }

    public class ErrorMessage
    {
        public string message { get; set; }
        public string code { get; set; }
    }
}
