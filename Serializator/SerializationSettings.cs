﻿using System.IO;
using System.Reflection;
using System.Text.Json;

namespace Serializator
{
    public static class SerializationSettings<T>
    {
        public static string Serialize(T settings)
        {
            return JsonSerializer.Serialize(settings);
        }
        public static T Deserialize(string json)
        {
            return JsonSerializer.Deserialize<T>(json);
        }

        public static void SaveSettingsToFile(T settings)
        {
            var jsonSettings = Serialize(settings);
            var settingsFilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "settings.json");
            File.WriteAllText(settingsFilePath, jsonSettings);
        }

        public static T GetSettingsFromFile()
        {
            var t = default(T);
            try
            {
                var settingsFilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "settings.json");
                var jsonSettings = File.ReadAllText(settingsFilePath);
                return Deserialize(jsonSettings);
            }
            catch
            {
                return t;
            }
        }
    }
}
