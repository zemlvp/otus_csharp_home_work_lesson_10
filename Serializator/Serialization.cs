﻿using System;
using System.Text.Json;

namespace Serializator
{
    public static class Serialization
    {
        public static string Serialize(Message message)
        {
            return JsonSerializer.Serialize(message);
        }
        public static Message Deserialize(string json)
        {
            return JsonSerializer.Deserialize<Message>(json);
        }
    }
}
